(ns clojure-getting-started.web
  (:require [compojure.core :refer [defroutes GET PUT POST DELETE ANY]]
            [compojure.handler :refer [site]]
            [compojure.route :as route]
            [clojure.java.io :as io]
            [ring.adapter.jetty :as jetty]
            [environ.core :refer [env]])
  (:import (java.time LocalDateTime)))

(def messages (atom []))

(defn reduce-messages [[user message date-time]]
  (str
    "(" (.toString (.toLocalDate date-time)) ":" (.toString (.toLocalTime date-time)) ") - "
    user " : " message "\n"))

(defn add-message [user message]
  (swap! messages conj [user message (LocalDateTime/now)]))

(defn get-messages []
  (map
    reduce-messages
    @messages))

(defroutes app
   (route/resources "/")
   (GET "/" []
      (slurp (io/resource "index.html")))
   (GET "/script.js" []
     (slurp (io/resource "script.js")))
   (GET "/getMessages" []
      (get-messages))
   (GET "/sendMessage" [user message]
     (add-message user message))
   (ANY "*" []
     (route/not-found (slurp (io/resource "404.html")))))

(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 5000))]
    (jetty/run-jetty (site #'app) {:port port :join? false})))

;; For interactive development:
;; (.stop server)
;; (def server (-main))
