
# clojure-instant-messanger-web-app-example

An instant messanger Clojure web app example using compojure and ring.

## Running Locally

Make sure you have Clojure and Leiningen installed.

```sh
$ git clone https://Ro-Rams@bitbucket.org/Ro-Rams/clojure-instant-messanger-web-app-example.git
$ cd clojure-instant-messanger-web-app-example
$ lein repl
user=> (require 'clojure-getting-started.web)
user=> (def server (clojure-getting-started.web/-main))
```
and to close the server
```sh
user=> (.stop server)
```

Your app should now be running on [localhost:5000](http://localhost:5000/).